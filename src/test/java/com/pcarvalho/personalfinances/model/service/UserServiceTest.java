package com.pcarvalho.personalfinances.model.service;

import com.pcarvalho.personalfinances.exception.BusinessException;
import com.pcarvalho.personalfinances.model.entity.User;
import com.pcarvalho.personalfinances.model.repository.UserRepository;
import com.pcarvalho.personalfinances.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Test(expected = Test.None.class)
    public void verifyEmail() {

        userRepository.deleteAll();
        userService.validateEmail("email@email.com");
    }

    @Test(expected = BusinessException.class)
    public void shouldLaunchExceptionWhenValidateEmailAlreadyExists() {
        User user = User.builder()
                .name("user")
                .email("email@email.com")
                .build();

        userRepository.save(user);
        userService.validateEmail("email@email.com");
    }
}