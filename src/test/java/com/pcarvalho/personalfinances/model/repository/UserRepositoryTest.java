package com.pcarvalho.personalfinances.model.repository;

import com.pcarvalho.personalfinances.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class UserRepositoryTest {

    @Autowired
    UserRepository repository;

    @Test
    public void verifyExistsEmail() {
        User user = User.builder()
                .name("user")
                .email("user@email.com")
                .build();

        repository.save(user);

        boolean result = repository.existsByEmail("user@email.com");

        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void verifyNotExistsEmail() {

        repository.deleteAll();

        User user = User.builder()
                .name("user")
                .email("user@email.com")
                .build();

        repository.save(user);

        boolean result = repository.existsByEmail("user1@email.com");

        Assertions.assertThat(result).isFalse();
    }
}