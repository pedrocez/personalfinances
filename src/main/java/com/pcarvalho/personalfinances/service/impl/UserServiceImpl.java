package com.pcarvalho.personalfinances.service.impl;

import com.pcarvalho.personalfinances.exception.AuthenticateException;
import com.pcarvalho.personalfinances.exception.BusinessException;
import com.pcarvalho.personalfinances.model.entity.User;
import com.pcarvalho.personalfinances.model.repository.UserRepository;
import com.pcarvalho.personalfinances.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User authenticate(String email, String password) {
        Optional<User> user = userRepository.findByEmail(email);

        if (!user.isPresent()) {
            throw new AuthenticateException("User not found.");
        }

        if (user.get().getPassword().equals(password)) {
            throw new AuthenticateException("Invalid password.");
        }
        return user.get();
    }

    @Override
    @Transactional
    public User saveUser(User user) {
        validateEmail(user.getEmail());
        return userRepository.save(user);
    }

    @Override
    public void validateEmail(String email) {
        boolean exists = userRepository.existsByEmail(email);
        if (exists) {
            throw new BusinessException("Already exists user email");
        }
    }
}
