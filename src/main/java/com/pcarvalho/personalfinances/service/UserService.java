package com.pcarvalho.personalfinances.service;

import com.pcarvalho.personalfinances.model.entity.User;

public interface UserService {

    User authenticate(String email, String password);

    User saveUser(User user);

    void validateEmail(String email);

}