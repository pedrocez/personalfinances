package com.pcarvalho.personalfinances.model.entity;

import com.pcarvalho.personalfinances.model.enums.LaunchStatusEnum;
import com.pcarvalho.personalfinances.model.enums.LaunchTypeEnum;
import lombok.*;
import org.springframework.data.convert.Jsr310Converters;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@Entity
@Table(name = "launch", schema = "finances")
public class Launch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "year")
    private Integer year;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @Column(name = "month")
    private Integer month;

    @Column(name = "value")
    private BigDecimal value;

    @Column(name = "creation_date")
    //@Convert(converter = Jsr310Converters.LocalDate.class)
    private LocalDate creationDate;

    @Column(name = "type")
    @Enumerated(value = EnumType.STRING)
    private LaunchTypeEnum typeEnum;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private LaunchStatusEnum statusEnum;
}