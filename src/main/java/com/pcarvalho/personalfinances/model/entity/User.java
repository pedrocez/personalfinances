package com.pcarvalho.personalfinances.model.entity;


import lombok.*;

import javax.persistence.*;

@Data
@Builder
@Entity
@Table(name = "user", schema = "finances")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;
}