package com.pcarvalho.personalfinances.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LaunchTypeEnum {
    PENDING,
    CANCELED,
    EFFECTED;
}