package com.pcarvalho.personalfinances.model.repository;

import com.pcarvalho.personalfinances.model.entity.Launch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LaunchRepository extends JpaRepository<Launch, Long> {

}