package com.pcarvalho.personalfinances.exception;

public class AuthenticateException extends RuntimeException {

    public AuthenticateException(String msg) {
        super();
    }
}